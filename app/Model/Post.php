<?php
namespace Albelli\Model;

class Post {

  /**
   * Load the assigned file for this Model
   */
  public function __construct() {
    $this->modelFile = json_decode(file_get_contents('Resources/posts.json'), false);
  }

  /**
   * Get all posts Model data from json file
   * @return array
   */
  public function getAll() {
    return $this->modelFile;
  }

  /**
   * Create new Post
   * @param  array $data
   * @return mixed
   */
  public function add($data) {

    $model = json_decode(file_get_contents('Resources/posts.json'), true); //make it assoc not object
    array_unshift($model['posts'], $data);
    $jsonData = json_encode($model);
    file_put_contents('Resources/posts.json', $jsonData);

  }

}
