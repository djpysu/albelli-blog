<?php

namespace Albelli\Core;

use Albelli\Traits\Singleton;

require_once PATH . 'Traits/SingletonTrait.php';

class Loader
{
    use Singleton;

    public function init()
    {
        // Register loader method for __autoload classes
        spl_autoload_register(array(__CLASS__, 'load'));
    }

    private function load($classItems)
    {
        $classItems = str_replace(array(__NAMESPACE__, 'Albelli', '\\'), '/', $classItems); // Remove namespace and backslash
        if (is_file(__DIR__ . '/' . $classItems . '.php'))
            require_once __DIR__ . '/' . $classItems . '.php';
        if (is_file(PATH . $classItems . '.php'))
            require_once PATH . $classItems . '.php';
    }
}
