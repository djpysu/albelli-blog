<?php
namespace Albelli\Core;

class Router
{
    public static function run ()
    {
        $namespace = 'Albelli\Controller\\';
        $defCtrl = $namespace . 'IndexController';
        call_user_func(array(new $defCtrl, 'notFound'));
    }

    public static function route($regex, $cb) {
      $regex = str_replace('/', '\/', $regex);
      $is_match = preg_match('/^' . ($regex) . '$/', $_SERVER['REQUEST_URI'], $matches, PREG_OFFSET_CAPTURE);
      if ($is_match) { $cb($matches); }
   }
}
