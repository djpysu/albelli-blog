<?php
namespace Albelli\Core;

class ViewManager
{
    /* get the view and the model methods based on the _get */
    public function getView($sViewName)
    {
        $this->_get($sViewName, 'View');
    }
    public function getModel($sModelName)
    {
        $this->_get($sModelName, 'Model');
    }

    /**
     * Main get method
     */
    private function _get($sFileName, $sType)
    {
        $sFullPath = '/' . $sFileName . '.php';
        if (is_file($sFullPath))
            require $sFullPath;
        else
            exit('The "' . $sFullPath . '" file doesn\'t exist');
    }

}
