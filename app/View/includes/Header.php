<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="ie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="ie lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="ie"> <![endif]-->
<!--[if IE 10]>        <html class="ie"> <![endif]-->
<!--[if gt IE 9]><!-->
<html> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?php echo \Albelli\Config\BlogConfig::SITE_NAME ?></title>

    <link rel="stylesheet" href="//cdn-files.cloud/arc/css/arc.discovery.min.css" />
    <link rel="stylesheet" href="Assets/css/main.css" />

    <!--[if IE 8]><link rel="stylesheet" href="//cdn-files.cloud/arc/css/arc-ie8fix.min.css" /><![endif]-->
    <script type='text/javascript' src="//cdn-files.cloud/arc/js/jquery.arc.min.js"></script>
    <script type='text/javascript' src="//cdn-files.cloud/arc/js/picturefill.arc.min.js"></script>
    <script type='text/javascript' src="Assets/js/main.js"></script>
</head>
<body>
