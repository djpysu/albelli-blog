<?php
  /**
   * Include the header app
   */
  require 'includes/Header.php';
?>
<header>
  <div class="row">
    <div class="small-12">
      <a href="/" id="wrap-logo" title="albelli.nl" data-tam="wrap-logo"><i class="icon logo albelli"></i></a>
    </div>
  </div>
</header>
<div class="row">
  <div class="small-12 columns" id="hero-shot-wrapper">
    <div class="image-block image-main-thumb">
        <a href="#" style="display: block;">
            <img src="Assets/images/blog-header.jpg" />
        </a>
    </div>
  </div>
</div>
<div class="row">
    <div class="small-12 columns text-center">
        <h1>Albelli Blog</h1>
        <h2 class="subheader">Not another custom blog based on custom PHP framework. MVC-style and JSON data files.</h2>
    </div>
</div>
<div class="row">
  <div class="small-12 columns">
    <form class="new-post" method="post" id="newPostForm" enctype="multipart/form-data">
      <?php
      /**
       * Generate a CSRF (cross-site request forgery) and save it in session
       */
      session_start();
      $csrfToken = md5(uniqid(mt_rand(),true));
      $_SESSION['csrfToken'] = $csrfToken;
      ?>
      <fieldset>
        <input type="hidden" id="csrfKey" name="csrfKey" value="<?php echo $csrfToken ?>" />
        <div class="small-12 columns">
          <h5>New blog post</h5>
        </div>
        <div class="small-12 columns">
          <div class="row">
            <div class="small-12 columns">
              <input name="title" type="text" placeholder="My post title" id="title" required />
              <textarea name="content" id="content" placeholder="The post content" required></textarea>
            </div>
          </div>
          <div class="row">
            <div class="small-6 columns">
              <input name="email" type="email" placeholder="Email Address" id="email" required/>
            </div>
          </div>
          <div class="row">
              <div class="small-12 columns">
                <input id="file" type="file" name="file" value="" accept="image/x-png,image/jpeg" required>
              </div>
          </div>
          <div class="row bottom-form">
              <div class="small-12 columns">
                <button type="submit" class="button">Save Post</button>
              </div>
          </div>
        </div>
      </fieldset>
    </form>
  </div>
</div>
<div class="row">
  <div class="medium-12 large-9 columns" id="postsList">
    <?php foreach ($this->data as $posts): ?>
      <?php foreach ($posts as $post) { ?>
        <div class="panel">
            <article class="post">
              <div class="row">
                <div class="small-12 medium-10 columns">
                  <h3><?php echo $post->title; ?></h3>
                </div>
                <div class="small-12 medium-2 columns">
                  <h6><?php echo $post->date; ?></h6>
                </div>
              </div>
              <div class="row">
                <div class="small-4 columns">
                  <img src="<?php echo $post->image; ?>">
                </div>
                <div class="small-8 columns">
                  <p><?php echo $post->description; ?></p>
                </div>
              </div>
            </article>
        </div>
      <?php } ?>
    <?php endforeach ?>
  </div>
  <div class="large-3 columns hide-for-medium hide-for-small">
    <div class="panel">
        <h5>Most used words</h5>
        <ul id="mostUsedWords">
          <?php foreach($this->tags as $key=>$tag) { ?>
            <li><?php echo $key."(".$tag.")"; ?></li>
          <?php } ?>
        </ul>
    </div>
  </div>
</div>


<div data-alert="" id="successFeedback" class="modal-alert green">
  <p>
    Your new post has been added!
  </p>
</div>


<?php
  /**
   * Include the footer app
   */
  require 'includes/Footer.php';
?>
