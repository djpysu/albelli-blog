<?php

namespace Albelli\Traits;

trait Singleton
{

    private static $instance = null;

    /**
     * Get instance of class.
     * @static
     * @return object Return the instance class or create first instance of the class.
     */
    public static function getInstance()
    {
        return isset(static::$instance)
          ? static::$instance
          : static::$instance = new static;
    }

    /* prevent more instances */
    private function __construct() {
      $this->init();
    }
    protected function init() {}
    private function __wakeup() {}
    private function __clone() {}
}
