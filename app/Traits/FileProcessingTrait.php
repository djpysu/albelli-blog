<?php

namespace Albelli\Traits;

trait FileProcessingTrait
{

    public static function uploadFile($file)
    {
      $target_dir = "Resources/";
      $target_file = $target_dir . basename($file["file"]["name"]);
      $uploadOk = 1;

      // Check if file already exists
      // if (file_exists($target_file)) {
      //     echo "Sorry, file already exists.";
      //     $uploadOk = 0;
      // }

      // Check file size
      if ($file["file"]["size"] > 500000) {
          $response =  "Sorry, your file is too large.";
          $uploadOk = 0;
      }

      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          $response = "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($file["file"]["tmp_name"], $target_file)) {
              $response =  "The file ". basename($file["file"]["name"]). " has been uploaded.";
              $response = 'Resources/'.basename($file["file"]["name"]);
          } else {
              $response =  "Sorry, there was an error uploading your file.";
          }
      }

      return $response;
    }
}
