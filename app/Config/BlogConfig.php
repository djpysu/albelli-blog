<?php
namespace Albelli\Config;
/**
 * Main config file - add final so nobody can override it
 * @var const
 */
final class BlogConfig
{
    const
    SITE_NAME = 'Albelli Blog',
    SITE_AUTHOR = 'Tudor-Radu Barbu',
    ADMIN_EMAIL = 'barbutudor@gmail.com';
}
