<?php
/**
 * @author           Tudor-Radu Barbu - barbutudor@gmail.com
 * @copyright        2017(C), Tudor-Radu Barbu. All Rights Reserved.
 * @version          0.1
 */

  namespace Albelli;

  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  if (version_compare(PHP_VERSION, '5.5.0', '<'))
      exit('This blog requires PHP 5.5 or higher. You have: ' . PHP_VERSION);
  if (!extension_loaded('mbstring'))
      exit('The script requires "mbstring" PHP extension. Please install it.');

  /* DEFINE CONSTANTS */
  define('HTTP_P', (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') ? 'https://' : 'http://');
  define('ROOT_URL', HTTP_P . $_SERVER['HTTP_HOST'] . str_replace('\\', '', dirname(htmlspecialchars($_SERVER['PHP_SELF'], ENT_QUOTES))) . '/'); // Remove backslashes for Windows compatibility
  define('PATH', __DIR__ . '/');

  try
  {
      require PATH . 'Core/Loader.php';
      Core\Loader::getInstance()->init(); // Load all classes
      Core\Router::route('/', function($matches){ Core\Router::run(); });
      Core\Router::route('/create', function($matches){ Controller\IndexController::create();  });

  }
  catch (\Exception $e)
  {
      echo $e->getMessage();
  }
