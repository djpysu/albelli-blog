<?php
namespace Albelli\Controller;

use Albelli\Traits\FileProcessingTrait;

/**
* The main (index) controller
*/
class IndexController extends BaseController {

  use FileProcessingTrait;
  protected $model, $data;

  public function __construct() {

    $this->getModel('Post');
    $this->model = new \Albelli\Model\Post;

    $this->index();
  }

  public function index() {
    $this->data = $this->model->getAll();
    $this->tags = $this->mostUsedWords($this->data->posts);
    $this->getView('Home');
  }

  public static function create()
  {
    header("Content-type: application/json");
    session_start();
    /**
     * errors & data to return
     * @var array
     */
    $errors         = array();
    $data           = array();

    /**
     * Error validation
     * @var mixed
     */


    if(isset($_FILES)) {
      $file = $_FILES;
    } else {
      $errors['file'] = 'No file here!';
    }
    if (empty($_POST['title'])) $errors['title'] = 'Title is required.';

    if (empty($_POST['content'])) $errors['content'] = 'Content is required.';

    if (empty($_POST['email'])) $errors['email'] = 'Email is required.';

    // If admin email
    if(isset($_POST['email']) && $_POST['email'] == \Albelli\Config\BlogConfig::ADMIN_EMAIL) $errors['email'] = 'This email is already used for other purposes';

    // Double check if is jpg/png image & upload it via Trait
    if(isset($_FILES['file'])) {

      $image_extensions_allowed = array('jpg', 'jpeg', 'png');
      $ext = strtolower(substr(strrchr($_FILES['file']['name'], "."), 1));
      if(!in_array($ext, $image_extensions_allowed)) {
        $errors['file'] = 'Image type is not valid.';
      }

      try{
        $response = IndexController::uploadFile($file);
        if (strpos($response, 'Resources') !== false) {
          $returnFile = $response;
        } else {
          $errors['file'] = $response;
          $returnFile = "";
        }

      } catch(Exception $e) {
        $errors['file'] =  $e->getMessage();
        $returnFile = "";
      }
    }

    // Check the CSRF token so we know if it's coming from index frontend
    if(isset($_POST['csrfKey']) && $_POST['csrfKey'] != $_SESSION['csrfToken']) {
       die("Unauthorized source!");
    } else {

      /**
       * Return errors & data messages back.
       * @var array
       */
      if (!empty($errors)) {
        $data['success'] = false;
        $data['errors']  = $errors;

      } else {

        /**
         * prepare date for saving the model
         */
        $loadData = ['title' => $_POST['title'], 'description' => $_POST['content'], 'date'=> gmdate('Y-m-d'), 'image' => $returnFile, 'tags' => 'tag', 'author' => $_POST['email']];

        try {
          $post = new \Albelli\Model\Post;
          $post->add($loadData);
          $data['post'] = $loadData;

        } catch (Exception $e) {
            $data['errors'] =  $e->getMessage();
        }
        $data['success'] = true;
        $data['message'] = 'Success!';
      }

      $lPost = new \Albelli\Model\Post;
      $lDoc = $lPost->getAll();
      $data['tags'] = IndexController::mostUsedWords($lDoc->posts);

      echo json_encode($data);
    }
  }

  /**
   * Get most used words in posts
   * @param  object $model
   * @return array
   */
  public static function mostUsedWords($model){
    $allStrings = "";
    foreach ($model as $post) {
      $allStrings .= $post->description.' ';
    }
    str_replace(".","",$allStrings);
    $array = explode(" ",strtolower($allStrings));
    $array = array_count_values($array);
    arsort($array);
    $result = array();

    foreach($array as $word=>$val) {
        if(strlen($word) > 4) {
            $result[$word] = $val; // push word into result array
        }
    }
    $newArray = array_slice($result, 0, 5, true);

    return $newArray;
  }

  public function notFound() {
  }
}
