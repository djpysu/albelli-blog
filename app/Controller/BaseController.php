<?php
namespace Albelli\Controller;

   /**
   * The base controller
   */
   class BaseController
   {

       public function getView($viewName)
       {
           $this->_get($viewName, 'View');
       }
       public function getModel($modelName)
       {
           $this->_get($modelName, 'Model');
       }

       /**
        * Main get method
        */
       private function _get($file, $type)
       {
           $upOne = realpath(__DIR__  . '../');
           $fullPath = $upOne . $type . '/' . $file . '.php';
           if (is_file($fullPath))
               require $fullPath;
           else
               exit('The "' . $fullPath . '" file doesn\'t exist');
       }


   }
