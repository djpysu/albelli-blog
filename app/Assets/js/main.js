$(document).ready(function() {

    $('#newPostForm').submit(function(e) {

      var formData = new FormData($("#newPostForm")[0]);
      // var formData = {
      //     'title'              : $('#postTitle').val(),
      //     'content'            : $('#postContent').val(),
      //     'email'              : $('#authorEmail').val(),
      //     'file'               : $('#file').val(),
      //     'csrfKey'            : $('#csrfKey').val()
      // };
      // $("#loading").show().css('visibility','visible');
      $.ajax({
            type        : 'POST',
            url         : '/create',
            data        : formData,
            dataType    : 'json',
            processData: false,
            encode      : true,
            contentType: false,
      })
      .done(function(data){
        /**
         * Handle errors
         * @param  array data
         * @return dom manipulation
         */
        if (!data.success) {
          if (data.errors.title) {
               $('#postTitle').addClass('error');
               $('#postTitle').before('<label class="left inline"><small class="error"><i class="small icon form-error"></i> ' + data.errors.title + '</small></label>');
           }
           if (data.errors.content) {
                $('#postContent').addClass('error');
                $('#postContent').before('<label class="left inline"><small class="error"><i class="small icon form-error"></i> ' + data.errors.content + '</small></label>');
           }
           if (data.errors.email) {
                $('#authorEmail').addClass('error');
                $('#authorEmail').before('<label class="left inline"><small class="error"><i class="small icon form-error"></i> ' + data.errors.email + '</small></label>');
           }
           if (data.errors.file) {
                $('#file').addClass('error');
                $('#file').before('<label class="left inline"><small class="error"><i class="small icon form-error"></i> ' + data.errors.file + '</small></label>');
           }
        } else {
          $("#successFeedback").show().delay(3000).fadeOut(); //show the alert

          /**
           * update the post list
           */
          var postUpdate = "<div class='panel'><article class='post'><div class='row'><div class='small-12 medium-10 columns'><h3>"+data.post.title+"</h3></div><div class='small-12 medium-2 columns'><h6>"+data.post.date+"</h6></div></div><div class='row'><div class='small-4 columns'><img src='"+data.post.image+"'></div><div class='small-8 columns'><p>"+data.post.description+"</p></div></div></article></div>";
          $("#postsList").prepend(postUpdate);
          $('#newPostForm').find("input[type=text], textarea").val(""); //reset the form, preventing user to submit the form multiple times

          /**
           * update most used words
           */
          $("#mostUsedWords").html("");
          $.each(data.tags, function(i, val) {
            $("#mostUsedWords").append("<li>" + i + "(" + val +")" + "</li>");
          });

        }
      })
      .fail(function(data){

      });

      e.preventDefault();

    });
});
