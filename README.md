# Albelli's blog

A custom PHP blog, MVC style and based on json files.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the interface and how to install them

- PHP > 5.5
- mbstring

Recommended PHP7

### Installing

A step by step series of examples that tell you have to get a development env running

1. No installation for developent stage

## Running the tests

TODO


## Built With

* PHP
* jQuery
* ARC - Albelli Resource Core - http://albumprinter.github.io/arc/
* HTML / css
* Ajax
* json

## Versioning

Made on Bitbucket

## Authors

* **Tudor Radu Barbu** - *Initial work* - barbutudor@gmail.com

## License

This project is licensed under the MIT License

## Acknowledgments
